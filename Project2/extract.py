# -*- coding: utf-8 -*-
# @Author: Heinrich Dinkel
# @Date:   2017-12-01 12:32:12
# @Last Modified by:   richman
# @Last Modified time: 2017-12-14 18:02:03

import numpy as np
import librosa
import argparse
import pywt
from tqdm import tqdm


def dumpcsv(fname, data):
    np.savetxt(fname, data, delimiter=",")


def dumpnp(fname, data):
    np.savez_compressed(fname, data)


def readcsv(fname):
    return np.loadtxt(fname, delimiter=',')


# TODO
def extract_cqcc(fname):
    cqttrans = librosa.core.cqt(*librosa.load(fname))


def extractfeature_fft(fname, n_fft, hop_length):
    wav, sr = librosa.load(fname, sr=None)
    S, nfft = librosa.core.spectrum._spectrogram(y=wav)
    return np.dot(librosa.filters.dct(13, S.shape[0]), S)


def extractfeature_imfcc(fname, n_fft, hop_length):
    wav, sr = librosa.load(fname, sr=None)
    S, n_fft = librosa.core.spectrum._spectrogram(
        y=wav, hop_length=hop_length, n_fft=n_fft, power=2)
    # Build a Mel filter, and inverse it with the moore-penrose pseudo
    mel_basis = np.linalg.pinv(librosa.filters.mel(sr, n_fft)).transpose()
    S = librosa.power_to_db(np.dot(mel_basis, S))
    return np.dot(librosa.filters.dct(13, S.shape[0]), S)


def extractfeature_spect(fname, n_fft, hop_length):
    wav, sr = librosa.load(fname, sr=None)
    S, n_fft = librosa.core.spectrum._spectrogram(
        y=wav, hop_length=hop_length, n_fft=n_fft, power=2)
    return librosa.power_to_db(S)


def extractfeature_db8(fname, n_fft, hop_length):
    wav, sr = librosa.load(fname, sr=None)
    S, nfft = librosa.core.spectrum._spectrogram(
        y=wav, hop_length=hop_length, n_fft=n_fft, power=2)
    S = librosa.power_to_db(S)
    cA, cD = pywt.dwt(S, 'db8')
    return cA


def extractfeature_db4(fname, n_fft, hop_length):
    wav, sr = librosa.load(fname, sr=None)
    S, nfft = librosa.core.spectrum._spectrogram(
        y=wav, hop_length=hop_length, n_fft=n_fft, power=2)
    S = librosa.power_to_db(S)
    cA, cD = pywt.dwt(S, 'db4')
    return cA


def extractfeature_cqt(fname, n_fft, hop_length):
    wav, sr = librosa.load(fname, sr=None)
    S = librosa.core.cqt(
        wav, sr=sr, hop_length=hop_length, n_bins=84, fmin=10.)
    return librosa.power_to_db(S)


def extractfeature_mfcc(fname, n_fft, hop_length):
    return librosa.feature.mfcc(*librosa.load(fname, sr=None), n_mfcc=13, n_fft=n_fft, hop_length=hop_length)


featuretypes = {'ChromaCQT': extractfeature_cqt, 'MFCC': extractfeature_mfcc,
                'FFT': extractfeature_fft, 'iMFCC': extractfeature_imfcc, 'db8': extractfeature_db8, 'db4': extractfeature_db4, 'spect': extractfeature_spect, 'cqt': extractfeature_cqt}

parser = argparse.ArgumentParser()
""" Arguments: inputfilelist, outputcsv """
parser.add_argument('inputfilelist', type=argparse.FileType(
    'r'), help="A simple filelist")
parser.add_argument('output', type=argparse.FileType(
    'w'), default="features.npz", nargs="?", help="Output file name, stored as npz file")
parser.add_argument('-type', default=["MFCC"], choices=featuretypes, nargs="+")
parser.add_argument('-nfft', default=400, type=int,
                    help="FFT window size. nfft/samplerate is the time in a window")
parser.add_argument('-shift', default=100, type=int,
                    help='FFT shift between two windows')

args = parser.parse_args()

fnames = [line.rstrip() for line in args.inputfilelist]

print("Extracting features {} for {} with shift {} and windowsize {}".format(
    " | ".join(args.type), args.inputfilelist.name, args.shift, args.nfft))


features = {}
# Extract multiple features for each line
for line in tqdm(fnames):
    # Linefeats is of size DIM, NSAMPLES
    linefeats = np.concatenate([featuretypes[f](line, args.nfft, args.shift)
                                for f in args.type], axis=0).astype(np.float32)
    # Transpose to obtain snampels , DIM
    linefeats = linefeats.transpose()
    features[line] = linefeats


dumpnp(args.output.name, features)
