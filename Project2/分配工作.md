# Resources

* Kaldi
* Matlab
* Librosa
* TensorFlow/Pytorch


# 特征

提CQT + FFT + Wavelet 特征 (按照 [ASVSpoof2017 Paper](https://arxiv.org/pdf/1705.08858.pdf))

格式: CSV. :

```python
def dumpcsv(fname, data):
    np.savetxt(fname, data, delimiter=",")
```


特征赋予后台群

R: 崔炜嗥, 高政风


# 后台

R: Jonas, 贾棋伟

按照 [ASVSpoof2017 Paper](https://arxiv.org/pdf/1705.08858.pdf) - Run their LCCN_FFT, but not that big (Maybe a result of ~ 12% Dev would be cool)


# Time

第十四周　－　结束提特征, 准备模型，准备文章　（就拷贝）
第十五 - 结束跑实验
16 - Write documentation, this time in Latex (Documentation/*.tx)


# Result

R: Heinrich

Just write a script outputting some score for each label, e.g.:

```python
name="File1"
score="0.52"
print("{} {}".format(name, score))

```


# Team

Heinrich Dinkel (Does nothing)
Jonas Wiedenmueller
贾棋伟
崔炜嗥
高政风