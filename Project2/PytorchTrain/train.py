# -*- coding: utf-8 -*-
# @Author: Heinrich Dinkel
# @Date:   2017-12-12 18:08:16
# @Last Modified by:   richman
# @Last Modified time: 2017-12-15 14:06:19

import argparse
import numpy as np
import models
import torch
import os
import torchnet as tnt
import logging
from tqdm import tqdm
from torch.autograd import Variable
from loss import FocalLoss
# Frame extension
from sklearn.feature_extraction import image


def createDataLabel(data, labels):
    concatdata, concatlabels = [], []
    for k in data:
        name = os.path.basename(k)
        assert name in labels, "Labels do not have {} in!".format(name)
        concatdata.append(data[k])
        # Generate the labels ( of the same length as samples)
        concatlabels.extend(np.full(len(data[k]), labels[name]))
    concatdata = np.concatenate(concatdata).astype(np.float32)
    return concatdata, torch.from_numpy(np.array(concatlabels))


def frameextension(data, fext):
    fextdim = fext * 2 + 1
    if fextdim == 1:
        return data
    # The extract patches method will not pad the first n and last n rows
    paddeddata = np.empty(
        (data.shape[0] + fext * 2, data.shape[1]), dtype=np.float32)
    paddeddata[:fext, :] = data[0]
    paddeddata[-fext:, :] = data[-1]
    paddeddata[fext:-fext, :] = data
    # Lazy mans frame extension, still fast enough!
    # Pads the input data with its adjacent (fextdim,fextdim) frames
    patches = image.extract_patches_2d(
        paddeddata, (fextdim, paddeddata.shape[-1])).reshape(-1, fextdim * paddeddata.shape[-1])
    return torch.from_numpy(patches) 


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('traindata', type=np.load,
                        help="extracted .npz file")
    parser.add_argument('trlabels', type=argparse.FileType(
        'r'), help="Labels for each training utterance")
    parser.add_argument('cvdata', type=np.load,
                        help="extracted .npz file")
    parser.add_argument('cvlabels', type=argparse.FileType('r'),
                        help="Labels for the cv data")
    parser.add_argument('-trcounts', type=str, default=None,
                        help="ark,t formatted counts (feat-to-len) for each utterance")
    parser.add_argument('-cvcounts', type=str, default=None,
                        help="ark,t formatted counts (feat-to-len) for each utterance")
    parser.add_argument('-bs', '--batchsize', type=int, default=256,
                        help="Training batchsize. Defaults to %(default)s")
    parser.add_argument('-output', default="trainedmodels/", type=str,
                        help="Output directory. Defaults to %(default)s. Needs to be empty")
    parser.add_argument('-lr', default=4e-5, type=float,
                        help="Starting learning rate. Defaults to %(default)s.")
    parser.add_argument('-epochs', default=200, type=int)
    parser.add_argument('-epochsize', default=0, type=int,
                        help="Number of batches to be seen for each epoch. Default full dataset.")
    parser.add_argument('-drop', default=0.0, type=float)
    parser.add_argument('-net', help="Neural network to use",
                        type=str, default="DNN")
    parser.add_argument('-cuda', default=False, action="store_true")
    parser.add_argument('-fext', default=5, type=int,
                        help="Context frame extension")
    args = parser.parse_args()

    # Magic
    traindata = args.traindata['arr_0'][()]
    cvdata = args.cvdata['arr_0'][()]
    # Handel trainlabels and traindata
    trainlabels = {line.split()[0]: line.split()[1] for line in args.trlabels}
    cvlabels = {line.split()[0]: line.split()[1] for line in args.cvlabels}

    uniquelabels = np.unique(list(trainlabels.values()))
    encoder = {lab: i for i, lab in enumerate(uniquelabels)}

    # Encode labels
    trainlabels = {k: encoder[lab] for k, lab in trainlabels.items()}
    cvlabels = {k: encoder[lab] for k, lab in cvlabels.items()}

    traindata, trainlabels = createDataLabel(
        traindata, trainlabels)
    cvdata, cvlabels = createDataLabel(cvdata, cvlabels)

    global_mean = np.mean(traindata,axis=0)
    global_std = np.std(traindata, axis=0)
    # Normalize train and dev data
    traindata = (traindata - global_mean) / global_std
    cvdata = (cvdata - global_mean) / global_std
    # Save the original number of feature dimensions
    nfeatures = traindata.shape[-1]
    # Extend frames
    traindata = frameextension(traindata, fext=args.fext)
    cvdata = frameextension(cvdata, fext=args.fext)


    

    # We summed all dimensions, actually useless but just for fun
    nsamples, dim = traindata.size()
    try:
        os.makedirs(args.output)
    except OSError:
        pass
    model = getattr(models, args.net)(dim,
                                      len(uniquelabels),
                                      dropout=args.drop,
                                      nfeatures=nfeatures)
    print("Creating new model {}".format(args.net))

    formatter = logging.Formatter(
        "[ %(levelname)s : %(asctime)s ] - %(message)s")
    logging.basicConfig(level=logging.DEBUG,
                        format="[ %(levelname)s : %(asctime)s ] - %(message)s")
    logger = logging.getLogger("Pytorch")
    # Dump log to file
    fh = logging.FileHandler(os.path.join(args.output, 'log'))
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    logger.info(model)

    logger.info("TrainData has mean {:=.2f} and Std {:=.2f}, Dev has mean {:=.2f} and std {:=.2f}".format(
        traindata.mean(), traindata.std(), cvdata.mean(), cvdata.std()))

    optimizer = torch.optim.SGD(
        model.parameters(), lr=args.lr, momentum=0.9, nesterov=True, weight_decay=1e-2)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
        optimizer, patience=0, verbose=True, factor=0.2)

    if args.cuda:
        model = model.cuda()

    crit = torch.nn.CrossEntropyLoss()

    def reset_meters():
        meter_loss.reset()
        time_meter.reset()
        accmeter.reset()

    def on_start(state):
        state['bestacc'] = 0
        state['bestloss'] = 1e20

    def on_forward(state):
        meter_loss.add(state['loss'].data[0])
        accmeter.add(*state['output'])

    # Function is not called during test! only training
    def on_start_epoch(state):
        # Put into training state
        model.train()
        reset_meters()
        state['iterator'] = tqdm(
            state['iterator'], unit='batch', leave=False)

    def trainf(sample):
        inputs, targets = Variable(sample[0]), Variable(sample[1])
        if args.cuda:
            inputs, targets = inputs.cuda(), targets.cuda()
        o = model(inputs)
        return crit(o, targets), (o.data, targets.data)

    def evalf(sample):
        inputs, targets = Variable(sample[0], volatile=True), Variable(
            sample[1], volatile=True)
        if args.cuda:
            inputs, targets = inputs.cuda(), targets.cuda()
        o = model(inputs)
        # Return o.data and targets.data for accmeter (in on_forward)
        return crit(o, targets), (o.data, targets.data)

    def on_end_epoch(state):
        message = 'Training Epoch {}: Time: {:=.2f}s/{:=.2f}m LR: {} Acc: {:=.2f} Loss (mean): {:=.4f} Loss (std): {:=.4f}'.format(
            state['epoch'], time_meter.value(), time_meter.value() / 60, optimizer.param_groups[0]['lr'], accmeter.value()[0], *meter_loss.value())
        logger.info(message)
        reset_meters()
        model.eval()
        engine.test(evalf, cvdataloader)

        logger.info("Epoch: {:=4} Acc: {:=.2f} % Bestacc: {:=.2f} % Time {:=3.2f}s LR: {:=3.1e} CVLoss (mean): {:=5.4f} CVLoss (std): {:=5.4f} ".format(
            state['epoch'], accmeter.value()[0], state['bestacc'], time_meter.value(), optimizer.param_groups[0]['lr'], *meter_loss.value()))
        scheduler.step(meter_loss.value()[0], epoch=state['epoch'])

        curacc = max(state['bestacc'], accmeter.value()[0])
        curloss = min(state['bestloss'], meter_loss.value()[0])
        if curacc > state['bestacc']:
            state['bestacc'] = curacc
            torch.save({'model': model, 'encoder': encoder, 'mean': global_mean, 'std': global_std},
                       os.path.join(args.output, 'model.th'))
        if curloss < state['bestloss']:
            state['bestloss'] = curloss
            torch.save({'model': model, 'encoder': encoder, 'mean': global_mean, 'std': global_std},
                       os.path.join(args.output, 'model_bestloss.th'))
        else:
            dump = torch.load(os.path.join(args.output, 'model_bestloss.th'))
            model.load_state_dict(dump['model'].state_dict())
        # Stop training if lr < 1e-6
        if optimizer.param_groups[0]['lr'] < 1e-8:
            state['epoch'] = 1e10
            return

        reset_meters()

    engine = tnt.engine.Engine()
    # Statistics
    time_meter = tnt.meter.TimeMeter(False)
    meter_loss = tnt.meter.AverageValueMeter()
    accmeter = tnt.meter.ClassErrorMeter(accuracy=True)

    engine.hooks['on_forward'] = on_forward
    engine.hooks['on_start'] = on_start
    engine.hooks['on_start_epoch'] = on_start_epoch
    engine.hooks['on_end_epoch'] = on_end_epoch

    traindataset = torch.utils.data.TensorDataset(traindata, trainlabels)
    traindataloader = torch.utils.data.DataLoader(
        traindataset, shuffle=True, batch_size=args.batchsize)

    cvdataset = torch.utils.data.TensorDataset(cvdata, cvlabels)
    cvdataloader = torch.utils.data.DataLoader(
        cvdataset, shuffle=False, batch_size=args.batchsize)
    engine.train(trainf, traindataloader, args.epochs, optimizer)


if __name__ == '__main__':
    main()
