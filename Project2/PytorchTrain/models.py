# -*- coding: utf-8 -*-
# @Author: Heinrich Dinkel
# @Date:   2017-12-12 19:28:43
# @Last Modified by:   richman
# @Last Modified time: 2017-12-15 13:07:50

import torch
import torch.nn as nn
import torch.nn.init as init
import torch.nn.functional as F
import numpy as np


class DNN(torch.nn.Module):
    def __init__(self, inputdim, noutputs, hiddim=512, nlayers=4, dropout=0.0):
        super(DNN, self).__init__()
        self.inputdim = inputdim
        self.noutputs = noutputs
        self.nlayers = nlayers
        self.dropout = dropout
        self.inputlayer = torch.nn.Linear(inputdim, hiddim)

        self.hidlayers = torch.nn.ModuleList()
        for _ in range(nlayers):
            self.hidlayers += [torch.nn.Linear(hiddim, hiddim, bias=False)]
            if self.dropout > 0.0:
                self.hidlayers += [torch.nn.Dropout(self.dropout)]
        self.outputlayer = torch.nn.Linear(hiddim, noutputs)
        # Watch out!
        # for m in self.modules():
        #     if isinstance(m, nn.Linear):
        #         init.kaiming_normal(m.weight, np.sqrt(2))
                # init.constant(m.bias, 0)

    def forward(self, x):
        x = F.relu(self.inputlayer(x), True)
        for i in range(self.nlayers):
            x = F.relu(self.hidlayers[i](x), True)
        return self.outputlayer(x)


class CNN(torch.nn.Module):
    """Hardcoded CNN, for 1001 * 11 input !"""

    def __init__(self, inputdim, noutputs, dropout=0.0):
        super(CNN, self).__init__()
        self.inputdim = inputdim
        self.noutputs = noutputs
        self.dropout = dropout

        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 64, 11),
            nn.BatchNorm2d(64),
            nn.MaxPool2d(3)
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(64, 64, 7),
            nn.BatchNorm2d(64),
            nn.MaxPool2d(3)
        )
        self.lin = nn.Sequential(
            nn.Linear(3840, 1024),
            nn.ReLU(True),
            nn.Linear(1024, 1024),
            nn.ReLU(True),
            nn.Linear(1024, 1024),
            nn.ReLU(True),
            nn.Linear(1024, noutputs)
        )

    def forward(self, x):
        batches, dim = x.size()
        assert dim == 1001 * 11
        x = x.view(batches, 1, 77, -1)
        x = self.conv1(x)
        x = self.conv2(x)
        x = x.view(batches, -1)
        return self.lin(x)


class MscaleCNN(torch.nn.Module):
    """Hardcoded MscaleCNN, for 1001 * 11 input !"""

    def __init__(self, inputdim, noutputs, dropout=0.0):
        super(MscaleCNN, self).__init__()
        self.inputdim = inputdim
        self.noutputs = noutputs
        self.dropout = dropout

        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 64, 11),
            nn.BatchNorm2d(64),
            nn.MaxPool2d(3)
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(64, 64, 7),
            nn.BatchNorm2d(64),
            nn.MaxPool2d(3)
        )
        self.conv3 = nn.Sequential(
            nn.Conv2d(64, 64, 3),
            nn.BatchNorm2d(64),
            nn.MaxPool2d(3)
        )
        self.classifer1 = nn.Sequential(
            nn.Linear(3840, 1024),
            nn.BatchNorm1d(1024),
            nn.ReLU(True),
            nn.Linear(1024, 1024),
            nn.ReLU(True),
            nn.Linear(1024, noutputs)
        )
        self.classifer2 = nn.Sequential(
            nn.Linear(192, 1024),
            nn.BatchNorm1d(1024),
            nn.ReLU(True),
            nn.Linear(1024, 1024),
            nn.ReLU(True),
            nn.Linear(1024, noutputs)
        )

    def forward(self, x):
        batches, dim = x.size()
        assert dim == 1001 * 11
        x = x.view(batches, 1, 77, -1)
        x = self.conv1(x)
        x = self.conv2(x)
        y1 = self.classifer1(x.view(batches, -1))
        x = self.conv3(x)
        y2 = self.classifer2(x.view(batches, -1))
        return y1 + y2


class RawCNN(torch.nn.Module):
    """Uses raw power, only 1D convolutions"""

    def __init__(self, inputdim, outputdim, **kwargs):
        super(RawCNN, self).__init__()
        self.inputdim = inputdim
        self.outputdim = outputdim
        self.kwargs = kwargs
        self.drop = kwargs.get('dropout', 0.0)
        mdllist = nn.ModuleList()
        lastfilt = 1
        curfilt = 16
        calcoutputdim = self.inputdim
        for i in range(8):
            # Last kernelsize is 2
            poolsize = 3 if i < 7 else 2
            mdllist += self.block(lastfilt, curfilt, poolsize=poolsize)
            if self.drop > 0.0:
                mdllist += [nn.Dropout(p=0.7, inplace=True)]
            lastfilt = curfilt
            if i < 2:
                curfilt = 16
            elif i < 4:
                curfilt = 64
            else:
                curfilt = 128
            calcoutputdim = int(1. * calcoutputdim / poolsize)
        assert calcoutputdim > 0, "Input dimension too small!"
        self.convs = nn.Sequential(*mdllist)
        self.fc = nn.Linear(curfilt * calcoutputdim, self.outputdim)


    def block(self, inp, out, kernelsize=3, poolsize=3):
        return [nn.Sequential(
            nn.Conv1d(inp, out, kernel_size=kernelsize, stride=1, padding=1),
            nn.BatchNorm1d(out),
            nn.ReLU(True),
            nn.MaxPool1d(poolsize)
        )]

    def forward(self, x):
        x = x.view(x.size(0), 1, self.inputdim)
        x = self.convs(x)
        x = x.view(x.size(0), -1)
        return self.fc(x)

class MFM(torch.nn.Module):
    """max-feature-map as described in 'A Light CNN for Deep Face Representation with Noisy Labels'"""

    def __init__(self, dim=1):
        super(MFM, self).__init__()
        self.dim = dim

    def forward(self, x):
        assert x.shape[self.dim] % 2 == 0, "MFM requires an even number of features."
        halves = torch.split(x, x.shape[self.dim]//2, dim=self.dim)
        return torch.max(halves[0], halves[1])


class LCNN(torch.nn.Module):
    """Modified LCNN as described in 'Audio replay attack detection with deep learning frameworks'."""

    def __init__(self, inputdim, outputdim, nfeatures=None, **kwargs):
        super(LCNN, self).__init__()
        self.inputdim = inputdim
        if nfeatures is None:
            raise ValueError("LCNN needs to know the original number of feature dimensions.")
        self.nfeatures = nfeatures
        assert self.inputdim % nfeatures == 0
        self.outputdim = outputdim
        self.kwargs = kwargs
        mdllist = nn.ModuleList()

        calcoutputdim = (self.inputdim//nfeatures, nfeatures)

        current_filter_size = 16
        mdllist.append(nn.Sequential(
            nn.Conv2d(1, 2*current_filter_size, kernel_size=5, stride=1, padding=2),
            MFM(),
            nn.MaxPool2d(2)))
        calcoutputdim = tuple(dim//2 for dim in calcoutputdim)

        filter_sizes = [
            (16, 24),
            (32, 32),
            (32, 16),
            (16, 16)]
        for hid, out in filter_sizes:
            mdllist += self.block(current_filter_size, hid, out)
            current_filter_size = out
            calcoutputdim = tuple(dim//2 for dim in calcoutputdim)
        assert all(dim > 0 for dim in calcoutputdim), "Input dimension too small! Got output of {}".format(calcoutputdim)

        self.convs = nn.Sequential(*mdllist)

        self.fcs = nn.Sequential(
            nn.Linear(current_filter_size * calcoutputdim[0]*calcoutputdim[1], 64),
            MFM(),
            nn.Linear(32, self.outputdim))


    def block(self, inp, hid, out, kernel_size=3, pool_size=2):
        return [nn.Sequential(
            nn.Conv2d(inp, 2*hid, kernel_size=1, stride=1),
            MFM(),
            nn.Conv2d(hid, 2*out, kernel_size=kernel_size, stride=1, padding=(kernel_size-1)//2),
            MFM(),
            nn.MaxPool2d(pool_size)
        )]

    def forward(self, x):
        batches, dim = x.size()
        x = x.view(batches, 1, -1, self.nfeatures)
        x = self.convs(x)
        x = x.view(batches, -1)
        return self.fcs(x)
