---
author:
- Heinrich Dinkel
- Jonas Windmüller
- 贾棋伟
- 崔炜嗥
- 高政风
title: State of the art spoofing detection
institute: 上海交通大学
fontsize: 12pt
date: \today
bibliography: ref.bib
csl: ieee.csl
CJKmainfont: "Droid Sans Fallback"
---


# Table of Contents

* Introduction - ASV and Spoofing (Heinrich Dinkel)
* Front-End Features (贾棋伟, 高政风)
* Backend Classifiers (Jonas Windmüller)
* Real-world spoofing detection - Additive Noise (崔炜嗥)

# ASV

Spoofing detection is within the domain of speaker verification.
![Speaker Recognition Workflow](SpeakerRecognitionFlow.png){ width=400px }

# ASV

* Goal: Access 
* Problem: Extract unique characteristics (features) for each speaker
* Task: Generate scores $Score(s)$ for each speaker $s$
* Errors: False Accept Error (FAR, Bad guy got in), False Reject Error (FRR, Right guy didn`t got in) given a threshold $\Lambda$
$$
\begin{aligned}
  P_{fa}(\Lambda) &= \frac{\left|\text{spoof trials with score} > \Lambda\right|}{\left|\ \text{all spoofed trials}\right|}\\
  P_{fr}(\Lambda) &= \frac{\left|\text{genuine trials with score} < \Lambda\right|}{\left|\text{all genuine trials}\right|}
\end{aligned}
$$
* Metric: Equal Error Rate $P_{fa} = P_{fr}$
* Approaches: GMM-UBM


# Spoofing - Task and Types


Imitate a given (enrolled) speaker, generating scores close to the real speaker ones, increasing FAR. 


### Four (idenified) types

* Mimic: A (professional) person mimics a target speaker ( hard to find people capable of this, not very big data driven )
* Replay: Record with an audio device (Phone) and replay recording to a system.
* Synthesis: Artificially generate data from text. 
* Voice conversion: Artificially generate data from (usually short) previous recordings of speech.

# Spoofing

![Spoofing](spoofing.png){width=400px}



# Presented Papers

* Front-end ( Features )
    * Generalization of Spoofing Countermeasures: A Case Study with ASVspoof 2015 and BTAS 2016 Corpora
    * Spectral Features for Synthetic Speech Detection
    * Articulation rate filtering of CQCC features for automatic speaker verification

* Backend
    * End-to-End spoofing detection with raw waveform CLDNNs
    * Small-footprint convolutional neural network for spoofing detection

* Noisy
    * Spoofing Goes Noisy: An Analysis of Synthetic Speech Detection in the Presence of Additive Noise
    * Deep Feature Engineering for Noise Robust Spoofing Detection