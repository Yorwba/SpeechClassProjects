---
author:
- Heinrich Dinkel
- Jonas Windmueller
- 贾棋伟
- 崔炜嗥
- 高政风
title: State of the art spoofing detection
institute: 上海交通大学
fontsize: 12pt
date: \today
bibliography: ref.bib
csl: ieee.csl
CJKmainfont: "Droid Sans Fallback"
---


# Table of Contents

* Introduction (Heinrich Dinkel)
* Front-End (贾棋伟, 高政风)
* Backend (Jonas Windmüller)
* Noisy (崔炜嗥)


# ASV

Spoofing detection is within the domain of speaker verification.

# Articulation rate filtering of CQCC features for automatic speaker verification

# Abstract

* A new articulation rate filter in combination with CQCC
* CQCC features are extracted with the CQT
* The resulting CQCC features are modelled using conventional techniques
* CQCCs generally outperform MFCCs  for a short-duration speaker verification scenario
* Relative reductions in equal error rates of as much as 60% compared to an MFCC baseline.


# Contributions

* The best reported spoofing detection results produced
* The first assessment of CQCCs for ASV and compares their performance to traditional MFCCs
* A new articulation rate filter tailored to CQCC analysis

# Constant Q Cepstral Coefficients

* The combination of the constant Q transform and cepstral analysis
* Offering a time-frequency resolution more closely related to that of human perception



## The constant Q transform

* The centre/bin frequencies of the CQT scale are geometrically distributed


 ![CQT](CQT.png)

* Q-factor: A measure of the filter selectivity and reflects the ratio between the center frequency and the bandwidth 

![Q-factor](Q-factor.png)

## CQCC extraction

* Spectrum obtained using DFT (Discrete Fourier Transform), whereas implemented with DCT (Discrete cosine transform)
* The cepstrum of a time sequence x(n) is obtained from the inverse transformation of the logarithm of the squared-magnitude spectrum
* Cepstrum maps N Fourier coefficients onto r independent, decorrelated cepstrum coefficients

 ![Cepstrum Maps](Cepstrum Maps.png)

* CQT frequency scale geometrically spaced, whereas DCT linearly spaced
* Spline interpolation method

 ![Extracting CQCC](Extracting CQCC.png)

# Articulation rate filtering

* Articulation rate filter tailored to CQCC features

## Motivation

* The success of relative spectral (RASTA) processing which emphasize the components of a signal that are typical of natural speech
* CQT offers greater resolution at lower frequencies
* New approach emphasise the components of a speech signal that are indicative of the articulation rate, designed adaptively for each utterance

## Pre-processing

* Focuses on a frequency region from $f_{min} = 0.5 Hz$,  to $f_{max} = 32Hz$

![ARTE filter design](ARTE filter design.png)

Among them:

$M = \frac{f_s}{10f_{max}}$

 ![ENV](ENV.png)

window $w(k)$ is defined as:

 ![Window](Window.png)

## Filter design and application to CQCCs

* We now can estimated the ARTE filter coefficients using a modified ARMA filter

The frequency response of the M-th order filter:

 ![Frequency response](Frequency response.png)

ARTE filtering for a real utterance and for different filter orders (All future work reported here was performed with an ARTE filter of order 3):

 ![Time-frequency](Time-frequency.png)

The application of ARTE filter to the temporal trajectory of the 20-th CQCC coefficient of the utterance in Figure 2

 ![Time](Time.png)

# Experimental Setup

## Database

* RSR2015 and RedDots, fixed pass-phrases
* Target-correct (TC) refers to trials where both the speaker and text match
* Target-wrong (TW) trial involves the target speaker but the incorrect pass-phrase
* Impostor-correct (IC) trial involves an impostor speaker but the correct pass-phrase
* Impostor-wrong (IW) trials, neither the speaker nor the text matches

![Datebase](Datebase.png)

## MFCC extraction

* Frame-blocked using a sliding window of 20 ms with a 10 ms shift
* Discrete Fourier transform applied to Hamming windowed frames to estimate the power spectrum
* 19th order MFCCs extracted using DCT of 20 log-power Mel-scaled filterbank outputs
* RASTA filtering
* Delta and delta-delta coefficients computed from the static parameter, resulting in feature vectors of dimension 57
* Speech activity detection (SAD)
* Cepstral mean and variance normalization

## CQCC extraction and ARTE filtering

* Extract Static CQCC coefficients of order 29 and process with ARTE filter
* SAD applied
* Delta coefficients appended, resulting in feature vectors of dimension 58
* Cepstral mean and variance normalization

## Classification and metrics

* Classifier is based upon conventional Gaussian mixture models (GMMs)
* A 512-component UBM is trained on the TIMIT database
* MAP adaptation is applied with a relevance factor of 10 and scores are the log-likelihood ratio given the target model and the UBM
* Performance is assessed in terms of equal error rate (EER)

# Results

## RSR2015

* CQCC-A shows the best performance among CQCC variants which is equivalent to that of MFCC-R features
* Logistic regression score fusion of MFCC-R and CQCC-A show significant improvements in performance across both development and evaluation sets
* Comparative results from the literature, fusion results compare more favorably

 ![RSR2015](RSR2015.png)

## RedDots

* results for MFCC and CQCC features show equivalent performance
* Fusion results deliver almost universally consistent improvements in performance

 ![RedDots](RedDots.png)

# Conclusions

* The first application of constant Q cepstral coefficients (CQCCs) to automatic speaker recognition
* A new articulation filter (ARTE)
* The ARTE filter performs an identical role to RASTA filtering applied to MFCCs
* CQCCs deliver equivalent performance to MFCCs for short-duration, text-dependent automatic speaker recognition
* With different time and frequency resolutions, CQCCs are complementary to MFCC features
