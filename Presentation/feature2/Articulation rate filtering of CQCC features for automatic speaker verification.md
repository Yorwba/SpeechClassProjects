
# Front-end Features - Ariculation rate filter for Cepstral Features


\begin{center}
\Huge Articulation rate filtering of CQCC features for automatic speaker verification
\end{center}

# Abstract

* A new articulation rate filter in combination with CQCC
* CQCC features are extracted with the CQT
* The resulting CQCC features are modelled using conventional techniques
* CQCCs generally outperform MFCCs  for a short-duration speaker verification scenario
* Relative reductions in equal error rates of as much as 60% compared to an MFCC baseline.


# Contributions

* The best reported spoofing detection results produced
* The first assessment of CQCCs for ASV and compares their performance to traditional MFCCs
* A new articulation rate filter tailored to CQCC analysis

# Constant Q Cepstral Coefficients

* The combination of the constant Q transform and cepstral analysis
* Offering a time-frequency resolution more closely related to that of human perception

<!-- # The constant Q transform

* The centre/bin frequencies of the CQT scale are geometrically distributed
 -->

<!-- ![CQT](feature2/CQT.png){width=300px}

* Q-factor: A measure of the filter selectivity and reflects the ratio between the center frequency and the bandwidth 


![Q-factor](feature2/Q-factor.png)

# CQCC extraction - 1

* Spectrum obtained using DFT (Discrete Fourier Transform), whereas implemented with DCT (Discrete cosine transform)
* The cepstrum of a time sequence x(n) is obtained from the inverse transformation of the logarithm of the squared-magnitude spectrum
* Cepstrum maps N Fourier coefficients onto r independent, decorrelated cepstrum coefficients

![Cepstrum Maps](feature2/Cepstrum Maps.png)

# CQCC extraction - 2

* CQT frequency scale geometrically spaced, whereas DCT linearly spaced
* Spline interpolation method

 ![Extracting CQCC](feature2/Extracting CQCC.png) -->

# Articulation rate filtering

* Articulation rate filter tailored to CQCC features

## Motivation

* The success of relative spectral (RASTA) processing which emphasize the components of a signal that are typical of natural speech
* CQT offers greater resolution at lower frequencies
* New approach emphasise the components of a speech signal that are indicative of the articulation rate, designed adaptively for each utterance

<!-- # Pre-processing

* Focuses on a frequency region from $f_{min} = 0.5 Hz$,  to $f_{max} = 32Hz$

![ARTE filter design](feature2/ARTE filter design.png)
 -->
<!-- # Pre-processing Cont.

Among those regions:

$M = \cfrac{f_s}{10f_{max}}$

![ENV](feature2/ENV.png){width=290px}

window $w(k)$ is defined as:

![Window](feature2/Window.png){width=290px}
 -->
# Filter design and application to CQCCs - Frequency response

* Estimate the ARTE filter coefficients using a modified ARMA filter

The frequency response of the M-th order filter:


$$
H(z) = \frac {\sum_{m=0}^{M} b_m z^{-m}}{1+\sum_{m=1}^M a_m z^{-m} }
$$
<!-- ![Frequency response](feature2/Frequency response.png){width=350px}
 -->

<!-- # Filter design and applicactions to CQCC - 2

ARTE filtering for a real utterance and for different filter orders (All future work reported here was performed with an ARTE filter of order 3):
 -->
<!-- #  Filter design and applicactions to CQCC - 3

![Time-frequency extraction](feature2/Time-frequency.png){width=250px}

# Filter design and applicactions to CQCC - 4

The application of ARTE filter to the temporal trajectory of the 20-th CQCC coefficient of the utterance in Figure 2

![Time Windowing](feature2/Time.png){width=360px} -->

# Experimental Setup - Database

* RSR2015 and RedDots, fixed pass-phrases
* Target-correct (TC) refers to trials where both the speaker and text match
* Target-wrong (TW) trial involves the target speaker but the incorrect pass-phrase
* Impostor-correct (IC) trial involves an impostor speaker but the correct pass-phrase
* Impostor-wrong (IW) trials, neither the speaker nor the text matches

# Experimental Setup - Database Cont.

![Datebase](feature2/Datebase.png)
<!-- 
#  Experimental Setup - MFCC extraction

* Frame-blocked using a sliding window of 20 ms with a 10 ms shift
* Discrete Fourier transform applied to Hamming windowed frames to estimate the power spectrum
* 19th order MFCCs extracted using DCT of 20 log-power Mel-scaled filterbank outputs
* RASTA filtering
* Delta and delta-delta coefficients computed from the static parameter, resulting in feature vectors of dimension 57
* Speech activity detection (SAD)
* Cepstral mean and variance normalization -->

<!-- #  Experimental Setup - CQCC extraction and ARTE filtering

* Extract Static CQCC coefficients of order 29 and process with ARTE filter
* SAD applied
* Delta coefficients appended, resulting in feature vectors of dimension 58
* Cepstral mean and variance normalization -->

#  Experimental Setup - Features, Classification and metrics

* 19th order MFCCs extracted using DCT of 20 log-power Mel-scaled filterbank outputs
* RASTA filtering
* Classifier is based upon conventional Gaussian mixture models (GMMs)
* A 512-component UBM is trained on the TIMIT database
* MAP adaptation is applied with a relevance factor of 10 and scores are the log-likelihood ratio given the target model and the UBM
* Performance is assessed in terms of equal error rate (EER)

# Results - RSR2015

* CQCC-A shows the best performance among CQCC variants which is equivalent to that of MFCC-R features
* Logistic regression score fusion of MFCC-R and CQCC-A show significant improvements in performance across both development and evaluation sets
* Comparative results from the literature, fusion results compare more favorably

# Results - RSR2015 Table

![RSR2015](feature2/RSR2015.png){width=200px}

# Results - RedDots

* Results for MFCC and CQCC features show equivalent performance
* Fusion results deliver almost universally consistent improvements in performance

 ![RedDots Result](feature2/RedDots.png)

# Conclusion

* The first application of constant Q cepstral coefficients (CQCCs) to automatic speaker recognition
* A new articulation filter (ARTE)
* The ARTE filter performs an identical role to RASTA filtering applied to MFCCs
* CQCCs deliver equivalent performance to MFCCs for short-duration, text-dependent automatic speaker recognition
* With different time and frequency resolutions, CQCCs are complementary to MFCC features