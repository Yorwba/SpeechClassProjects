

# Noisy Spoofing Detection

\begin{center}
What happens in real world scenarios?
\end{center}

# Current Methods for Noise Robust Spoofing Detection - Feature

* Same features as for traditional Spoofing detection, including:
    - Mel-frequency Cepstral Coefficients (MFCCS)
    - Inverted Mel-frequency Cepstral Coefficients (IMFCCs)
    - Spectral Centroid Magnitude Coefficients (SCMCs)
    - Constant Q Cepstral Coefficients (CQCCS)
    - Mean Hilbert Envelope Coefficients (MHECs)
    - Relative-Phase Shift (RPS) Features
    - Modified Group Delay Function
    - Cosine Phase (CosPhase) Features


<!-- # Feature post-processing steps

- Deltas
- Cepstral mean normalization

# Speech enhancement techniques

- Magnitude spectral substraction
- Power spectral substraction
- Winener filtering -->

# Effect of Feature Post-Processing
![](noisy/picture/post_processing.png)
Effects of $\Delta$ and $\Delta\Delta$ MFCC features and Cepstral Mean Subtraction on synthetic speech detection. First row, MFCC features. Second row, CosPhase Features.

# Comparison of features
![](noisy/picture/performance.png)
Comparison (EER, %) of different front-end features in noisy conditions on development set using Gaussian Mixture Model classifier. The results for clean original condition are presented as well as the average results for all noisy sub-conditions.

<!-- # Effect of Speech Enhancement
![](noisy/picture/speech_enhancement_techniques.png)
DET curves for different speech enhancement techniques under additive noise (0 dB). First row, MFCC features. Second row, CosPhase features.
 -->
<!-- # i-vector Countermeasures from Different Features
![](noisy/picture/i_vector.png)
Comparison (EER, %) of different front-end features in noisy conditions on development set using Cosine/Probabilistic Linear Discriminant Analysis i-vector classifiers. The results for the clean (original) condition are presented as well as the average results for all noisy sub-conditions. The lower half of the table presents a difference between the corresponding EERs for PLDA and cosine scoring. Blue cells indicate conditions where PLDA scoring is advantageous to cosine scoring, whereas red cells indicate the opposite.
 -->
# GMM outperforms superior to other scoring variants
![](noisy/picture/GMM_and_i_vector.png)
Comparison (EER, %) of Gaussian Mixture Model classifier and two i-vector based classifiers: Cosine scoring and Probabilistic Linear Discriminant Analysis. We consider individual attacks on clean evaluation set using selected two magnitude (MFCCs and SCMC) and two phase (RPS and MGD) based features.

<!-- # Magnitude features (MFCCs and SCMCs) yield lower EERs
![](noisy/picture/magnitude_features_yield_lower_EERs.png)
 -->

# Neural networks for deep feature
- Deep neural networks
- Recurrent neural networks
- Convolutional neural networks

# Neural networks for deep feature Cont.

![Layer of feature extraction](noisy/picture/deep_feature.png){width=250px}

<!-- # Enhancement for noise robustness
- Multi-condition training
- Noise-aware training
- Annealed Dropout training
 -->
# Noise-aware training for deep models - DNN / BLSTM
![](noisy/picture/noise_aware_for_dnn_or_blstm.jpg)

Noise-aware training for DNN and BLSTM

# Noise-aware training for deep models - CNN

![](noisy/picture/noise_aware_for_cnn.jpg)

Noise-aware training for CNN
<!-- 
# Implemention of annealed dropout
In annealed dropout, the dropout probability of the nodes in the network is decreased as training progresses.


$prob[t] = \max(0, 1-\frac{t}{N}) \times prob[0]$ -->

# Comparison of clean-condition, multi-condition and annealed dropout

![](noisy/picture/clean_multi_annealed.png)
The EERs(%) comparison of DNN-based deep features, which utilized defferent training strategies, including clean-condition training, multi-condition training and annealed dropout training

# Comparison of different deep features

![Feature comparision](noisy/picture/deep_models.png){width=300px}

# Comparison of different deep features with noise-aware training

![Noise aware training](noisy/picture/noise_aware.png){width=300px}

# Combination within different deep features

![Different features](noisy/picture/combination.png){width=350px}

# Thanks

\begin{center}
\Huge Thanks!
\end{center}